# NOBUGS 2024 Satellite Meeting: Introduction to data acquisition and on-line data analysis with BLISS and EWOKS

**Conference registration**: https://indico.esrf.fr/event/114/overview

**Audience**: [scientists and IT professionals working on software for X-ray, neutron and muon sources](https://www.nobugsconference.org/)

**Objective**: demonstrate the usage of different parts of the Bliss Eco System

![Bliss Exo System](ecosystem.excalidraw.png "Bliss Exo System")

## Program

1. Introduce the different components of the Bliss Eco System
   - `bliss`: acquisition and hardware control
   - `blissdata`: publish and read acquisition data, during and after the experiment
   - `blisswriter`: save acquisition data in NeXus-compliant HDF5 files using `blissdata`
   - `flint`: visualize acquisition data in a desktop GUI using `blissdata`
   - `daiquiri`: acquisition control and acquisition data visualization in a web GUI using `bliss` and `blissdata`
   - `ewoks`: process acquisition data or automate acquisition control

2. Bliss: description (+ demo?)

3. Blissdata + Blisswriter: [description + demo](https://gitlab.esrf.fr/bliss/bliss/-/tree/master/scripts/scanning/withoutbliss)

4. Flint: description + demo

5. Daquiri: description (+ demo?)

6. Ewoks: [description + demo](https://ewoksfordevs.readthedocs.io/en/stable/#/89) + Flint custom plots

> :warning: I don't know about `blissterm`
